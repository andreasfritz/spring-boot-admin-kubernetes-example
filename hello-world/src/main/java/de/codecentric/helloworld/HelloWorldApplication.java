package de.codecentric.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class HelloWorldApplication extends SpringBootServletInitializer {

  public static void main(String... args) {
    var app = new SpringApplication(HelloWorldApplication.class);
    app.setApplicationStartup(new BufferingApplicationStartup(2048));
    app.run();
  }

}
